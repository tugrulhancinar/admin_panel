import 'dart:convert';

import 'package:admin_panel/product/model/base_model.dart';

class MezarlikModel extends BaseModel {
  MezarlikModel({
    this.mezarId,
    this.mezarAd,
  });

  int mezarId;
  String mezarAd;

  factory MezarlikModel.fromJson(String str) =>
      MezarlikModel.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory MezarlikModel.fromMap(Map<String, dynamic> json) => MezarlikModel(
        mezarId: json["mezarId"] == null ? null : json["mezarId"],
        mezarAd: json["mezarAd"] == null ? null : json["mezarAd"],
      );

  Map<String, dynamic> toMap() => {
        "mezarId": mezarId == null ? null : mezarId,
        "mezarAd": mezarAd == null ? null : mezarAd,
      };

  @override
  fromJson(Map<String, dynamic> json) => MezarlikModel.fromMap(json);
}
