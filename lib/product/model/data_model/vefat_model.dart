// To parse this JSON data, do
//
//     final vefatModel = vefatModelFromMap(jsonString);

import 'dart:convert';

class VefatModel {
  VefatModel({
    this.vefatId,
    this.mezarId,
    this.ad,
    this.soyad,
    this.babaAd,
    this.dTarihi,
    this.oTarihi,
    this.il,
    this.ada,
    this.parsel,
    this.enlem,
    this.boylam,
    this.resim,
  });

  final int vefatId;
  final int mezarId;
  final String ad;
  final String soyad;
  final String babaAd;
  final DateTime dTarihi;
  final DateTime oTarihi;
  final String il;
  final int ada;
  final int parsel;
  final double enlem;
  final double boylam;
  final String resim;

  VefatModel copyWith({
    int vefatId,
    int mezarId,
    String ad,
    String soyad,
    String babaAd,
    DateTime dTarihi,
    DateTime oTarihi,
    String il,
    int ada,
    int parsel,
    double enlem,
    double boylam,
    String resim,
  }) =>
      VefatModel(
        vefatId: vefatId ?? this.vefatId,
        mezarId: mezarId ?? this.mezarId,
        ad: ad ?? this.ad,
        soyad: soyad ?? this.soyad,
        babaAd: babaAd ?? this.babaAd,
        dTarihi: dTarihi ?? this.dTarihi,
        oTarihi: oTarihi ?? this.oTarihi,
        il: il ?? this.il,
        ada: ada ?? this.ada,
        parsel: parsel ?? this.parsel,
        enlem: enlem ?? this.enlem,
        boylam: boylam ?? this.boylam,
        resim: resim ?? this.resim,
      );

  factory VefatModel.fromJson(String str) =>
      VefatModel.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory VefatModel.fromMap(Map<String, dynamic> json) => VefatModel(
        vefatId: json["vefatId"],
        mezarId: json["mezarId"],
        ad: json["ad"],
        soyad: json["soyad"],
        babaAd: json["babaAd"],
        dTarihi: DateTime.parse(json["dTarihi"]),
        oTarihi: DateTime.parse(json["oTarihi"]),
        il: json["il"],
        ada: json["ada"],
        parsel: json["parsel"],
        enlem: json["enlem"].toDouble(),
        boylam: json["boylam"].toDouble(),
        resim: json["resim"],
      );

  Map<String, dynamic> toMap() => {
        "mezarId": mezarId,
        "ad": ad,
        "soyad": soyad,
        "babaAd": babaAd,
        "dTarihi": dTarihi.toIso8601String(),
        "oTarihi": oTarihi.toIso8601String(),
        "il": il,
        "ada": ada,
        "parsel": parsel,
        "enlem": enlem,
        "boylam": boylam,
        "resim": resim,
      };

  @override
  String toString() {
    return 'VefatModel{vefatId: $vefatId, mezarId: $mezarId, ad: $ad, soyad: $soyad, babaAd: $babaAd, dTarihi: $dTarihi, oTarihi: $oTarihi, il: $il, ada: $ada, parsel: $parsel, enlem: $enlem, boylam: $boylam, resim: $resim}';
  }
}
