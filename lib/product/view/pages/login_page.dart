import 'package:admin_panel/core/components/app/page_state.dart';
import 'package:admin_panel/core/components/widget/alet_dialog.dart';
import 'package:admin_panel/core/constant/navigation/navigation_constants.dart';
import 'package:admin_panel/core/init/navigation/navigation_services.dart';
import 'package:admin_panel/helper.dart';
import 'package:admin_panel/product/view_model/user_view_model.dart';
import 'package:flutter/material.dart';
import 'package:admin_panel/core/extension/context_extension.dart';
import 'package:get/instance_manager.dart';
import 'package:get/state_manager.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  String mail, password;
  bool prefixState = true;

  final cont = Get.put(UserViewModel());
  final formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar,
      body: GetBuilder<UserViewModel>(
        builder: (_) => cont.state == PageState.BUSY
            ? Center(child: CircularProgressIndicator())
            : body,
      ),
    );
  }

  AppBar get buildAppBar {
    return AppBar(
      centerTitle: true,
      title: Text("Giriş"),
    );
  }

  Widget get body => Center(
        child: Container(
          padding: context.paddingHigh,
          width: context.width / 2,
          child: Form(
            key: formKey,
            child: Column(
              children: [
                mailTextFormField,
                context.emptyNormalWidget,
                passwordTextFormField,
                context.emptyNormalWidget,
                loginButton,
              ],
            ),
          ),
        ),
      );

  Container get loginButton {
    return Container(
      width: context.width / 4,
      child:
          ElevatedButton(onPressed: loginButtononPressed, child: Text("Giriş")),
    );
  }

  Widget get mailTextFormField {
    return TextFormField(
      validator: ValidatorHelper.emailValidator,
      onSaved: mailSave,
      decoration: InputDecoration(
        labelText: "Mail",
        suffix: Opacity(
            opacity: 0,
            child: IconButton(
                onPressed: null, icon: Icon(Icons.ac_unit_outlined))),
        border: OutlineInputBorder(),
      ),
    );
  }

  Widget get passwordTextFormField {
    return TextFormField(
      validator: ValidatorHelper.passwordValidator,
      onSaved: passwordSave,
      obscureText: prefixState,
      decoration: InputDecoration(
        labelText: "Şifre",
        suffix: IconButton(
          icon: passwordTextFormFieldIcon,
          onPressed: () {
            setState(() {
              prefixState = !prefixState;
            });
          },
        ),
        border: OutlineInputBorder(),
      ),
    );
  }

  Widget get passwordTextFormFieldIcon => prefixState
      ? Icon(Icons.no_encryption_gmailerrorred_rounded, color: Colors.blue)
      : Icon(Icons.lock, color: Colors.blue);

  mailSave(String txt) => mail = txt;

  passwordSave(String txt) => password = txt;

  loginButtononPressed() {
    if (formKey.currentState.validate()) {
      formKey.currentState.save();

      cont.login(mail, password).catchError((e) {
        CustomAlertDialog(
          baslik: "Giriş Hatası",
          bodyText: e.details.values.first,
        ).goster(context);
      }).then((value) {
        if (value) {
          NavigationServices.instance
              .navigateToPage(path: NavigationConstans.HOME_PAGE);
        } else {}
      });
    }
  }
}
