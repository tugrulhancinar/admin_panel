import 'package:flutter/material.dart';
import 'package:admin_panel/core/extension/context_extension.dart';

class NotFoundPage extends StatelessWidget {
  const NotFoundPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Text(
          "Böyle bir sayfa yok",
          style: context.textTheme.headline5,
        ),
      ),
    );
  }
}
