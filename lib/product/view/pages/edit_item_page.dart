import 'package:admin_panel/core/components/app/page_state.dart';
import 'package:admin_panel/core/components/widget/alet_dialog.dart';
import 'package:admin_panel/core/extension/context_extension.dart';
import 'package:admin_panel/helper.dart';
import 'package:admin_panel/product/model/data_model/vefat_model.dart';
import 'package:admin_panel/product/view_model/mezarlik.dart';
import 'package:admin_panel/product/view_model/user_view_model.dart';
import 'package:file_picker_cross/file_picker_cross.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:get/instance_manager.dart';

class EditItemPage extends StatefulWidget {
  final VefatModel vefatModel;

  const EditItemPage({Key key, @required this.vefatModel}) : super(key: key);

  @override
  _ItemAddPageState createState() => _ItemAddPageState();
}

class _ItemAddPageState extends State<EditItemPage> {
  int selectedItem, ada, parsel;
  String ad, soyAd, babaAd, il, resim;
  double enlem, boylam;

  FilePickerCross resimFile;
  DateTime dTarihi, oTarihi;
  bool isOpen = false;

  VefatModel vefatModel;

  final dTarihiCont = TextEditingController();
  final oTarihiCont = TextEditingController();
  final formKey = GlobalKey<FormState>();
  final cont = Get.put(UserViewModel());
  final mezarCont = Get.put(MezarViewModel());

  @override
  void initState() {
    super.initState();
    if (mezarCont.mezarlikList == null) mezarCont.getAllMezarlik;
    vefatModel = widget.vefatModel;
    dTarihi = vefatModel.dTarihi;
    oTarihi = vefatModel.oTarihi;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: GetBuilder<UserViewModel>(
        builder: (_) => cont.state == PageState.BUSY
            ? Center(child: CircularProgressIndicator())
            : body,
      ),
    );
  }

  Container get body {
    if (selectedItem == null) {
      for (int i = 0; i < mezarCont.mezarlikList.length; i++) {
        if (vefatModel.mezarId == mezarCont.mezarlikList[i].mezarId) {
          selectedItem = i;
        }
      }
    }

    return Container(
      alignment: Alignment.topCenter,
      padding: EdgeInsets.symmetric(horizontal: context.width / 4),
      child: bodyListView,
    );
  }

  Widget get bodyListView {
    return Form(
      key: formKey,
      child: SingleChildScrollView(
        child: Column(
          children: [
            context.emptyMediumWidget,
            Text("Güncelle", style: context.textTheme.headline6),
            context.emptyNormalWidget,
            image,
            context.emptyNormalWidget,
            buildDropDownMenu,
            context.emptyNormalWidget,
            textFormField(
                "Ad", vefatModel.ad, nameOnSave, ValidatorHelper.nameValidator),
            context.emptyNormalWidget,
            textFormField("Soyad", vefatModel.soyad, soyAdOnSave,
                ValidatorHelper.surNameValidator),
            context.emptyNormalWidget,
            textFormField("Baba Adı", vefatModel.babaAd, babaAdOnSave,
                ValidatorHelper.nameValidator),
            context.emptyNormalWidget,
            textFormField(
                "İl", vefatModel.il, ilOnSave, ValidatorHelper.nameValidator),
            context.emptyNormalWidget,
            textFormField("Ada", vefatModel.ada.toString(), adaOnSave,
                ValidatorHelper.numberValidator,
                numberMode: true),
            context.emptyNormalWidget,
            textFormField("Parsel", vefatModel.parsel.toString(), parselOnSave,
                ValidatorHelper.numberValidator,
                numberMode: true),
            context.emptyNormalWidget,
            textFormField("Enlem", vefatModel.enlem.toString(), enlemOnSave,
                ValidatorHelper.numberValidator,
                numberMode: true),
            context.emptyNormalWidget,
            textFormField("Boylam", vefatModel.boylam.toString(), boylamOnSave,
                ValidatorHelper.numberValidator,
                numberMode: true),
            context.emptyNormalWidget,
            dTarihiTextFormField,
            context.emptyNormalWidget,
            oTarihiTextFormField,
            resimEkleButton,
            context.emptyNormalWidget,
            saveButton,
            context.emptyNormalWidget,
          ],
        ),
      ),
    );
  }

  Container get image {
    return Container(
      alignment: Alignment.center,
      child: resimFile == null
          ? Image.network(vefatModel.resim)
          : Image.memory(resimFile.toUint8List()),
    );
  }

  Widget get resimEkleButton => Container(
        alignment: Alignment.centerLeft,
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(
              primary: Colors.white,
              shadowColor: context.customBoxShadow1Blue.first.color),
          child: Padding(
            padding: context.paddingLowMedium,
            child: Text(
              resimFile == null ? "Resim Ekle" : "Resim Eklendi",
              style: context.textTheme.bodyText2.copyWith(
                color: resimFile == null ? Colors.black : Colors.green,
              ),
            ),
          ),
          onPressed: () {
            resimEkle();
          },
        ),
      );

  Widget get saveButton => Container(
        width: context.width,
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(
              shadowColor: context.customBoxShadow1Blue.first.color),
          child: Padding(
            padding: context.paddingLowMedium,
            child: Text(
              "Güncelle",
              style: context.textTheme.bodyText2.copyWith(color: Colors.white),
            ),
          ),
          onPressed: () {
            savebuttonOnPressed();
          },
        ),
      );

  Widget get buildDropDownMenu => DropdownButtonHideUnderline(
        child: Container(
          width: context.width,
          padding: context.paddingNormalHorizontal,
          decoration: BoxDecoration(
            color: Theme.of(context).primaryColor,
            borderRadius: context.containerBorderRadiusLow,
          ),
          child: DropdownButton<int>(
            style: TextStyle(color: Colors.grey),

            isExpanded: true,
            items: dropDownButtonItems,
            onTap: () {
              setState(() {
                isOpen = !isOpen;
              });
            },
            elevation: 24,
            //dropdownColor: context.colorScheme.anaMaviRenk,
            icon: Icon(
              CupertinoIcons.chevron_down,
              size: 24,
              color: Colors.white,
            ),
            value: selectedItem,
            onChanged: changeCemetery,
          ),
        ),
      );

  Widget textFormField(String labelText, String value, FormFieldSetter onSaved,
          FormFieldValidator<String> validate,
          {bool numberMode: false}) =>
      TextFormField(
        onSaved: onSaved,
        initialValue: value,
        validator: validate,
        inputFormatters: numberMode
            ? <TextInputFormatter>[
                // FilteringTextInputFormatter.digitsOnly,
                FilteringTextInputFormatter.allow(RegExp('[0-9.,]+')),
              ]
            : null,
        decoration: InputDecoration(
          labelText: labelText,
          border: OutlineInputBorder(),
        ),
      );

  Widget get dTarihiTextFormField => Padding(
        padding: context.paddingLowVertical,
        child: TextFormField(
            onTap: () => dateTimePick(true),
            controller: dTarihiCont,
            readOnly: true,
            onSaved: dateTextFormFieldSave,
            decoration: inputDecoration(
              dTarihi != null
                  ? Helper.dateFormatGunAyYil(dTarihi)
                  : "Doğum Tarihi seçilmedi",
              "Doğum Tarihi",
            )),
      );

  Widget get oTarihiTextFormField => Padding(
        padding: context.paddingLowVertical,
        child: TextFormField(
            onTap: () => dateTimePick(false),
            controller: oTarihiCont,
            readOnly: true,
            onSaved: dateTextFormFieldSave,
            decoration: inputDecoration(
              oTarihi != null
                  ? Helper.dateFormatGunAyYil(oTarihi)
                  : "Vefat tarihi seçilmedi",
              "Vefat Tarihi",
            )),
      );

  InputDecoration inputDecoration(String hintText, String helperTXT) {
    return InputDecoration(
        hintText: hintText,
        border: outlineInputBorder,
        helperText: helperTXT,
        enabledBorder: outlineInputBorder,
        focusedBorder: outlineInputBorder,
        fillColor: Colors.blue.withOpacity(0.1),
        filled: true);
  }

  OutlineInputBorder get outlineInputBorder => OutlineInputBorder(
        borderRadius: BorderRadius.circular(5),
        borderSide: BorderSide(width: 0, color: Colors.blue.withOpacity(0.1)),
      );

  List<DropdownMenuItem> get dropDownButtonItems {
    return textList
        .asMap()
        .map((i, e) => MapEntry(
            i,
            DropdownMenuItem<int>(
                value: i,
                child: Text(e,
                    style: context.textTheme.bodyText2.copyWith(
                        fontWeight: FontWeight.w100, color: Colors.black)))))
        .values
        .toList();
  }

  List<String> get textList =>
      mezarCont.mezarlikList.map((e) => e.mezarAd).toList();

  void changeCemetery(int i) {
    setState(() {
      print(selectedItem);
      selectedItem = i;
    });
  }

  void nameOnSave(txt) => ad = txt;

  void soyAdOnSave(txt) => soyAd = txt;

  void babaAdOnSave(txt) => babaAd = txt;

  void ilOnSave(txt) => il = txt;

  void adaOnSave(txt) => ada = txt.runtimeType.toString() == "int"
      ? int.parse(txt)
      : double.parse(txt.toString().replaceAll(",", ".")).toInt();

  void parselOnSave(txt) {
    parsel = txt.runtimeType.toString() == "int"
        ? int.parse(txt)
        : double.parse(txt.toString().replaceAll(",", ".")).toInt();
  }

  void enlemOnSave(txt) {
    enlem = double.parse(txt.toString().replaceAll(",", "."));
  }

  void boylamOnSave(txt) {
    boylam = double.parse(txt.toString().replaceAll(",", "."));
  }

  void dateTextFormFieldSave(date) {}

  void savebuttonOnPressed() async {
    if (formKey.currentState.validate()) {
      formKey.currentState.save();
      var a = await cont
          .updateVefat(
        vefatModel.copyWith(
            ad: ad,
            ada: ada,
            babaAd: babaAd,
            boylam: boylam,
            dTarihi: dTarihi,
            enlem: enlem,
            il: il,
            oTarihi: oTarihi,
            parsel: parsel,
            soyad: soyAd,
            mezarId: mezarCont.mezarlikList[selectedItem].mezarId,
            resim: resimFile != null
                ? Helper.convertFileToBase64(resimFile)
                : vefatModel.resim),
      )
          .catchError((e) {
        CustomAlertDialog(
          baslik: "Hata",
          bodyText: e.details.values.first,
        ).goster(context);
      });

      if (a != null) {
        setState(() {
          vefatModel = a;
        });
        ScaffoldMessenger.of(context).showSnackBar(
            Helper.snackBarSuccessful("Başarılı ", "Güncelleme başarılı"));

        Navigator.pop(context);
      }
    }
  }

  void resimEkle() async {
    FilePickerCross myFile = await FilePickerCross.importFromStorage(
        type: FileTypeCross.image,
        fileExtension:
            'png, jpg, jpeg' // Only if FileTypeCross.custom . May be any file extension like `dot`, `ppt,pptx,odp`
        );

    if (myFile != null) {
      setState(() {
        resimFile = myFile;
      });
    }
  }

  dateTimePick(bool dogumBool) async {
    Helper.convertFileToBase64(resimFile);
    var secilenTarih = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime(1800),
        lastDate: DateTime.now().add(Duration(days: 2)));
    setState(() {
      if (dogumBool) {
        dTarihi = secilenTarih.add(Duration(days: 1));
      } else {
        oTarihi = secilenTarih.add(Duration(days: 1));
      }
    });
  }
}
