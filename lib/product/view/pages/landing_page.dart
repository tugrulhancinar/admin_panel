import 'package:admin_panel/product/view/pages/login_page.dart';
import 'package:admin_panel/product/view_model/mezarlik.dart';
import 'package:admin_panel/product/view_model/user_view_model.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class LandingPage extends StatelessWidget {
  LandingPage({Key key}) : super(key: key);
  final cont = Get.put(UserViewModel());
  final mezarCont = Get.put(MezarViewModel());

  @override
  Widget build(BuildContext context) {
    if (mezarCont.mezarlikList == null) mezarCont.getAllMezarlik;

    return LoginPage();
  }
}
