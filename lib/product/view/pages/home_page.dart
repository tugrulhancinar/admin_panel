import 'package:admin_panel/core/components/app/page_state.dart';
import 'package:admin_panel/core/components/widget/alet_dialog.dart';
import 'package:admin_panel/core/constant/navigation/navigation_constants.dart';
import 'package:admin_panel/core/init/navigation/navigation_services.dart';
import 'package:admin_panel/helper.dart';
import 'package:admin_panel/product/model/data_model/vefat_model.dart';
import 'package:admin_panel/product/view/pages/search.dart';
import 'package:admin_panel/product/view_model/user_view_model.dart';
import 'package:flutter/material.dart';
import 'package:admin_panel/core/extension/context_extension.dart';
import 'package:get/instance_manager.dart';
import 'package:get/state_manager.dart';

class HomePage extends StatefulWidget {
  HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final List<TableRow> widgetList = [];
  final cont = Get.put(UserViewModel());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(centerTitle: true, title: Text("Başlık")),
      body: GetBuilder<UserViewModel>(
        builder: (_) => cont.state == PageState.BUSY
            ? Center(child: CircularProgressIndicator())
            : body,
      ),
    );
  }

  Container get body => Container(
        padding: context.paddingLowHorizontal,
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              context.emptyNormalWidget,
              textFieldAndButton,
              context.emptyNormalWidget,
              buildTable,
            ],
          ),
        ),
      );

  Widget get textFieldAndButton => Container(
        width: context.width,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(width: context.width / 4, child: textField),
            // Container(),
            Container(
              width: context.width / 8,
              child: elevatedButton,
            ),
          ],
        ),
      );

  ElevatedButton get elevatedButton => ElevatedButton(
        child: Text("Yeni kayıt ekle"),
        onPressed: () {
          NavigationServices.instance
              .navigateToPage(path: NavigationConstans.ADD_ITEM_PAGE);
        },
      );

  Widget get textField => TextField(
        readOnly: true,
        onTap: () {
          showSearch(context: context, delegate: CustomSearch());
        },
        decoration: InputDecoration(
          contentPadding: EdgeInsets.all(context.normalValue),
          suffixIcon: IconButton(
            icon: Icon(Icons.search),
            onPressed: () {
              // cont.search(searchTextCont.text);
              showSearch(context: context, delegate: CustomSearch());
            },
          ),
          hintText: "Ara",
        ),
      );

  Widget get buildTable {
    return Container(
      width: context.width,
      child: DataTable(
        columns: [
          dataColumn("Ad-Soyad"),
          dataColumn("Baba Adi"),
          dataColumn("Doğum Tarihi"),
          dataColumn("Ölüm Tarihi"),
          dataColumn("Ada"),
          dataColumn("Parsel"),
          dataColumn("Enlem"),
          dataColumn("Boylam"),
          dataColumn("İşlem"),
        ],
        rows: cont.vefatModelList
            .map(
              (e) => DataRow(
                cells: [
                  dataCell(e.ad + " " + e.soyad),
                  dataCell(e.babaAd),
                  dataCell(Helper.dateFormatGunAyYil(e.dTarihi)),
                  dataCell(Helper.dateFormatGunAyYil(e.oTarihi)),
                  dataCell(e.ada.toString()),
                  dataCell(e.parsel.toString()),
                  dataCell(e.enlem.toString()),
                  dataCell(e.boylam.toString()),
                  DataCell(icons(e))
                ],
              ),
            )
            .toList(),
      ),
    );
  }

  DataColumn dataColumn(String txt) => DataColumn(label: Text(txt));

  DataCell dataCell(String txt) => DataCell(Text(
        txt,
        overflow: TextOverflow.ellipsis,
      ));

  Row icons(VefatModel vefatModel) => Row(
        children: [
          editButton(vefatModel),
          deleteButton(vefatModel),
        ],
      );

  IconButton editButton(VefatModel vefatModel) => IconButton(
        icon: Icon(Icons.edit, color: context.theme.primaryColor),
        onPressed: () {
          NavigationServices.instance.navigateToPage(
              path: NavigationConstans.EDIT_ITEM_PAGE, data: vefatModel);
        },
      );

  IconButton deleteButton(VefatModel vefatModel) => IconButton(
        icon: Icon(
          Icons.delete,
          color: Colors.red[800],
        ),
        onPressed: () {
          CustomAlertDialog(
            bodyText: vefatModel.ad +
                " isimli kaydı silmek istediğinize emin misiniz?",
            baslik: "Sil",
            actions: [
              TextButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: Text("Vazgeç")),
              TextButton(
                  onPressed: () {
                    Navigator.pop(context);
                    cont.deleteVefat(vefatModel).catchError((e) {
                      CustomAlertDialog(
                        baslik: "Hata",
                        bodyText: e.details.values.first,
                      ).goster(context);
                    }).then((value) {
                      if (value) {
                        ScaffoldMessenger.of(context).showSnackBar(
                            Helper.snackBarSuccessful(
                                "Başarılı", "Silme işlemi başarılı"));
                      }
                    });
                  },
                  child: Text("Sil")),
            ],
          ).goster(context);
        },
      );

  Widget titleText(String txt) => Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text(
          txt,
          style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
          textAlign: TextAlign.center,
        ),
      );

  Widget tableItemText(String txt) => Padding(
        padding: const EdgeInsets.all(8),
        child: Text(
          txt,
          style: TextStyle(fontSize: 16),
          overflow: TextOverflow.ellipsis,
          textAlign: TextAlign.center,
        ),
      );
}
