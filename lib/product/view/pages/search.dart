import 'package:admin_panel/core/components/widget/alet_dialog.dart';
import 'package:admin_panel/core/constant/navigation/navigation_constants.dart';
import 'package:admin_panel/core/init/navigation/navigation_services.dart';
import 'package:admin_panel/helper.dart';
import 'package:admin_panel/product/model/data_model/vefat_model.dart';
import 'package:admin_panel/product/view_model/user_view_model.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:turkish/turkish.dart';

class CustomSearch extends SearchDelegate {
  final userCont = Get.put(UserViewModel());
  List<VefatModel> mylist = [];

  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      IconButton(
          icon: Icon(Icons.close),
          onPressed: () {
            query = "";
          })
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return buildLeadingIconButton(context);
  }

  IconButton buildLeadingIconButton(BuildContext context) {
    return IconButton(
      icon: Icon(Icons.arrow_back),
      onPressed: () {
        close(context, null);
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    mylist = listele;

    return mylist.isEmpty ? buildCenter(context) : buildTable(context);
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    mylist = listele;

    return mylist.isEmpty ? buildCenter(context) : buildTable(context);
  }

  List<VefatModel> get listele => query.isEmpty
      ? userCont.vefatModelList
      : userCont.vefatModelList.where((element) {
          return element.ad.toLowerCaseTr().contains(query.toLowerCaseTr()) ||
              element.soyad.toLowerCaseTr().contains(query.toLowerCaseTr()) ||
              (element.ad + element.soyad)
                  .replaceAll(' ', '')
                  .toLowerCaseTr()
                  .contains((query.toLowerCase().replaceAll(' ', '')));
        }).toList();

  Center buildCenter(BuildContext context) => Center(
        child: Text('Sonuç yok',
            style: context.theme.textTheme.bodyText2
                .copyWith(color: Colors.black)),
      );

  Widget buildTable(BuildContext context) {
    return Container(
      width: context.width,
      child: DataTable(
        columns: [
          dataColumn("Ad-Soyad"),
          dataColumn("Baba Adi"),
          dataColumn("Doğum Tarihi"),
          dataColumn("Ölüm Tarihi"),
          dataColumn("Ada"),
          dataColumn("Parsel"),
          dataColumn("Enlem"),
          dataColumn("Boylam"),
          dataColumn("İşlem"),
        ],
        rows: mylist
            .map(
              (e) => DataRow(
                cells: [
                  dataCell(e.ad + " " + e.soyad),
                  dataCell(e.babaAd),
                  dataCell(Helper.dateFormatGunAyYil(e.dTarihi)),
                  dataCell(Helper.dateFormatGunAyYil(e.oTarihi)),
                  dataCell(e.ada.toString()),
                  dataCell(e.parsel.toString()),
                  dataCell(e.enlem.toString()),
                  dataCell(e.boylam.toString()),
                  DataCell(icons(e, context))
                ],
              ),
            )
            .toList(),
      ),
    );
  }

  DataColumn dataColumn(String txt) => DataColumn(label: Text(txt));

  DataCell dataCell(String txt) => DataCell(Text(
        txt,
        overflow: TextOverflow.ellipsis,
      ));

  Row icons(VefatModel vefatModel, BuildContext context) => Row(
        children: [
          editButton(vefatModel, context),
          deleteButton(vefatModel, context),
        ],
      );

  IconButton editButton(VefatModel vefatModel, BuildContext context) =>
      IconButton(
        icon: Icon(Icons.edit, color: context.theme.primaryColor),
        onPressed: () {
          NavigationServices.instance.navigateToPage(
              path: NavigationConstans.EDIT_ITEM_PAGE, data: vefatModel);
        },
      );

  IconButton deleteButton(VefatModel vefatModel, BuildContext context) =>
      IconButton(
        icon: Icon(
          Icons.delete,
          color: Colors.red[800],
        ),
        onPressed: () {
          CustomAlertDialog(
            bodyText: vefatModel.ad +
                " isimli kaydı silmek istediğinize emin misiniz?",
            baslik: "Sil",
            actions: [
              TextButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: Text("Vazgeç")),
              TextButton(
                  onPressed: () {
                    Navigator.pop(context);
                    userCont.deleteVefat(vefatModel).catchError((e) {
                      CustomAlertDialog(
                        baslik: "Hata",
                        bodyText: e.details.values.first,
                      ).goster(context);
                    }).then((value) {
                      if (value) {
                        ScaffoldMessenger.of(context).showSnackBar(
                            Helper.snackBarSuccessful(
                                "Başarılı", "Silme işlemi başarılı"));
                      }
                    });
                  },
                  child: Text("Sil")),
            ],
          ).goster(context);
        },
      );
}
