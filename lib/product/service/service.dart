import 'dart:convert';
import 'package:admin_panel/core/constant/service/api_const.dart';
import 'package:admin_panel/product/model/data_model/mezarlik_model.dart';
import 'package:admin_panel/product/model/data_model/vefat_model.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;

class MyServices {
  static MyServices _instace;

  static MyServices get instance => _instace ??= MyServices._init();

  MyServices._init();

  final header = {"Content-Type": "application/json"};

  Future<bool> login(String mail, String pass) async {
    Map map = {
      "mail": mail,
      "parola": pass,
    };

    var result = await http.post(
      ApiConstants.instance.login,
      body: jsonEncode(map),
      headers: header,
    );
    if (result.statusCode == 201) {
      return true;
    } else {
      throw PlatformException(
          code: result.body, details: jsonDecode(result.body));
    }
  }

  Future<List<VefatModel>> get gelAllVefat async {
    var result =
        await http.get(ApiConstants.instance.getAllVefat, headers: header);
    List<VefatModel> list = [];
    if (result.statusCode != 200) {
      var allMap = jsonDecode(result.body);
      list = [];
      if (result.body.isNotEmpty) {
        for (var currentMap in allMap) {
          list.add(VefatModel.fromMap(currentMap));
        }
      }
    } else {
      throw PlatformException(
          code: result.body,
          details: jsonDecode(utf8.decode(result.bodyBytes)));
    }

    return list;
  }

  Future<VefatModel> updateVefat(VefatModel vefatModel) async {
    VefatModel v;
    var result = await http.post(
      ApiConstants.instance.updateVefat(vefatModel.vefatId.toString()),
      body: jsonEncode(vefatModel.toMap()),
      headers: header,
    );
    if (result.statusCode == 201) {
      var allMap = jsonDecode(result.body);
      for (var currentMap in allMap.values) {
        v = VefatModel.fromMap(currentMap);
      }
      return v;
    } else {
      throw PlatformException(
          code: result.body,
          details: jsonDecode(utf8.decode(result.bodyBytes)));
    }
  }

  Future<List<MezarlikModel>> get getAllMezarlik async {
    var result = await http.get(ApiConstants.instance.tumMezaliklar);
    List<MezarlikModel> list;
    if (result.statusCode != 200) {
      var allMap = jsonDecode(result.body);
      list = (allMap as List<dynamic>)
          .map((e) => MezarlikModel.fromMap(e))
          .toList();
    } else {
      throw PlatformException(
          code: result.body,
          details: jsonDecode(utf8.decode(result.bodyBytes)));
    }

    return list;
  }

  Future<bool> deleteVefat(VefatModel vefatModel) async {
    var result = await http.delete(
        ApiConstants.instance.deleteVefat(vefatModel.vefatId.toString()));

    if (result.statusCode == 201) {
      return true;
    } else {
      return false;
    }
  }

  Future<int> createVefat(VefatModel vefatModel) async {
    var result = await http.post(
      ApiConstants.instance.vefatCreate,
      body: jsonEncode(vefatModel.toMap()),
      headers: header,
    );
    int sonuc;

    if (result.body ==
        '"Arithmetic overflow error converting numeric to data type numeric."') {
      throw PlatformException(
          code: result.body,
          details:
              "Hatalı numara girişi. Lütfen girdiğiniz numaraları kontrol edin.");
    }

    if (result.statusCode == 201) {
      var allMap = jsonDecode(result.body)[0];
      for (var currentMp in allMap.values) {
        sonuc = currentMp;
      }

      return sonuc;
    } else {
      throw PlatformException(
          code: result.body, details: jsonDecode(result.body));
    }
  }
}
