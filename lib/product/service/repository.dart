import 'package:admin_panel/product/model/data_model/mezarlik_model.dart';
import 'package:admin_panel/product/model/data_model/vefat_model.dart';
import 'package:admin_panel/product/service/service.dart';

enum AppMode { DEBUG, RELEASE }

class ServiceRepository {
  final AppMode appMode = AppMode.RELEASE;

  static ServiceRepository _instace;

  static ServiceRepository get instance =>
      _instace ??= ServiceRepository._init();

  ServiceRepository._init();

  Future<bool> login(String mail, String pass) async {
    if (appMode == AppMode.RELEASE) {
      var result = await MyServices.instance.login(mail, pass);
      return result;
    } else {
      return null;
    }
  }

  Future<List<VefatModel>> get getAllVefat async {
    if (appMode == AppMode.RELEASE) {
      return await MyServices.instance.gelAllVefat;
    } else {
      return null;
    }
  }

  Future<VefatModel> updateVefat(VefatModel vefatModel) async {
    if (appMode == AppMode.RELEASE) {
      print("updateVefat");
      return await MyServices.instance.updateVefat(vefatModel);
    } else {
      return null;
    }
  }

  Future<List<MezarlikModel>> get getAllMezarlik async {
    if (appMode == AppMode.RELEASE) {
      return await MyServices.instance.getAllMezarlik;
    } else {
      return null;
    }
  }

  Future<bool> deleteVefat(VefatModel vefatModel) async {
    if (appMode == AppMode.RELEASE) {
      return await MyServices.instance.deleteVefat(vefatModel);
    } else {
      return null;
    }
  }

  Future<int> createVefat(VefatModel vefatModel) async {
    if (appMode == AppMode.RELEASE) {
      return await MyServices.instance.createVefat(vefatModel);
    } else {
      return null;
    }
  }
}
