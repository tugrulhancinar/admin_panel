import 'package:admin_panel/core/components/app/page_state.dart';
import 'package:admin_panel/product/model/data_model/mezarlik_model.dart';
import 'package:admin_panel/product/service/repository.dart';
import 'package:get/get_state_manager/get_state_manager.dart';

class MezarViewModel extends GetxController {
  var _state = PageState.IDLE;

  List<MezarlikModel> _mezarlikList;

  List<MezarlikModel> get mezarlikList => _mezarlikList;

  set state(PageState value) {
    _state = value;
    update();
  }

  PageState get state => _state;

  Future<List<MezarlikModel>> get getAllMezarlik async {
    state = PageState.BUSY;
    _mezarlikList = await ServiceRepository.instance.getAllMezarlik;
    state = PageState.IDLE;
    return mezarlikList;
  }
}
