import 'package:admin_panel/core/components/app/page_state.dart';
import 'package:admin_panel/product/model/data_model/vefat_model.dart';
import 'package:admin_panel/product/service/repository.dart';
import 'package:get/get_state_manager/get_state_manager.dart';

class UserViewModel extends GetxController {
  var _state = PageState.IDLE;
  List<VefatModel> _list;

  UserViewModel() {
    getAllVefat;
  }

  set state(PageState value) {
    _state = value;
    update();
  }

  PageState get state => _state;

  List<VefatModel> get vefatModelList => _list;

  Future<bool> createVefat(VefatModel vefatModel) async {
    state = PageState.BUSY;
    var result = false;
    int id = await ServiceRepository.instance.createVefat(vefatModel);

    if (id != null) {
      _list.add(vefatModel.copyWith(vefatId: id));
      result = true;
    }
    state = PageState.IDLE;
    return result;
  }

  Future<List<VefatModel>> get getAllVefat async {
    state = PageState.BUSY;
    _list = (await ServiceRepository.instance.getAllVefat).reversed.toList();
    state = PageState.IDLE;
    return vefatModelList;
  }

  Future<VefatModel> updateVefat(VefatModel vefatModel) async {
    state = PageState.BUSY;
    var result = await ServiceRepository.instance.updateVefat(vefatModel);

    for (int i = 0; i < _list.length; i++) {
      if (_list[i].vefatId == vefatModel.vefatId) {
        _list[i] = result;
        print("değiştirildi değiştirilen i: $i vefatid: " +
            _list[i].vefatId.toString());
      }
    }

    state = PageState.IDLE;
    return result;
  }

  Future<bool> deleteVefat(VefatModel vefatModel) async {
    state = PageState.BUSY;
    var result = await ServiceRepository.instance.deleteVefat(vefatModel);
    if (result) {
      _list.remove(vefatModel);
      for (int i = 0; i < _list.length; i++) {
        if (_list[i].vefatId == vefatModel.vefatId) {
          _list.removeAt(i);
        }
      }
    }
    state = PageState.IDLE;
    return result;
  }

  Future<bool> login(String mail, String pass) async {
    state = PageState.BUSY;
    var result;
    try {
      result = await ServiceRepository.instance.login(mail, pass);
    } finally {
      state = PageState.IDLE;
    }

    return result;
  }
}
