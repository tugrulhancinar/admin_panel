import 'package:admin_panel/product/view/pages/landing_page.dart';
import 'package:flutter/material.dart';
import 'core/init/navigation/navigation_route.dart';
import 'core/init/navigation/navigation_services.dart';

void main() {
  runApp(MyApp());
}

//todo Mezar listesini çek

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Admin Panel',
      navigatorKey: NavigationServices.instance.navigatorKey,
      onGenerateRoute: NavigationRoute.instance.generateRoute,
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: LandingPage(),

      //    home: HomePage(),
    );
  }
}
