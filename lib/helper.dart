import 'package:file_picker_cross/file_picker_cross.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class Helper {
  static SnackBar snackBarError(String title, String bodyText) {
    return SnackBar(
      backgroundColor: Colors.white,
      duration: Duration(seconds: 2),
      content: Row(
        children: <Widget>[
          Icon(
            Icons.error_outline,
            color: Colors.red[800],
          ),
          SizedBox(
            width: 20,
          ),
          Expanded(
            child: Text(
              bodyText,
              style: TextStyle(
                color: Colors.red[800],
              ),
            ),
          )
        ],
      ),
    );
  }

  static String dateFormatGunAyYil(DateTime date) {
    return DateFormat('dd-MM-yyyy').format(date);
  }

  static SnackBar snackBarSuccessful(String title, String bodyText) {
    return SnackBar(
      backgroundColor: Color(0xFFF57E20),
      duration: Duration(seconds: 2),
      content: Row(
        children: <Widget>[
          Icon(Icons.check, color: Colors.white),
          SizedBox(width: 20),
          Expanded(
            child: Text(
              bodyText,
              style: TextStyle(color: Colors.white),
            ),
          )
        ],
      ),
    );
  }

  static String convertFileToBase64(FilePickerCross file) {
    if (file == null) return null;
    String uzanti = file.path.split('.').last;
    String base64Image = "data:image/$uzanti;base64," + file.toBase64();
    return base64Image;
    /*
    if (file == null) return null;
    String uzanti = file.path.split('.').last;
    List<int> imageBytes = file.readAsBytesSync();
    String base64Image =
        "data:image/$uzanti;base64," + base64Encode(imageBytes);
    return base64Image;
    */
  }
}

class ValidatorHelper {
  static String _emailRegex =
      r"^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-3-.]+$";

  static String passwordValidator(String value) {
    if (value.isEmpty || value == null) {
      return "Parola boş olamaz";
    } else if (value.length < 6) {
      return "Parolanız en az 6 karekter olmalı";
    } else if (value.length > 20) {
      return "Parolanız en fazla 20 karakter olabilir";
    } else {
      return null;
    }
  }

  static String nameValidator(String txt) {
    if (txt.isEmpty) {
      return "Soy Adı boş bırakılamaz";
    } else if (txt.length < 2) {
      return "Hatalı giriş";
    }
    return null;
  }

  static String numberValidator(txt) {
    double ada = double.parse(txt.toString().replaceAll(",", "."));
    if (txt.isEmpty) {
      return "Bu alan boş bırakılamaz";
    } else if (ada < 0) {
      return "Hatalı giriş";
    }
    return null;
  }

  static String surNameValidator(String txt) {
    if (txt.isEmpty) {
      return "Soy Adı boş bırakılamaz";
    } else if (txt.length < 2) {
      return "Hatalı giriş";
    }
    return null;
  }

  static String phoneNumberValidator(String value) {
    if (value.isEmpty) {
      return "Email kısmı boş bırakılamaz";
    } else if (value.length < 11 || value.length > 11) {
      return "Geçersiz telefon numarası";
    }
    return null;
  }

  static String emailValidator(String value) {
    //return null;
    RegExp _regExp = new RegExp(_emailRegex);
    if (value.isEmpty) {
      return "Email kısmı boş bırakılamaz";
    } else if (!_regExp.hasMatch(value))
      return "Hatalı Mail formatı";
    else {
      return null;
    }
  }

  static String dogumValidator(String value) {
    if (value == "" || value == "null") {
      return null;
    } else {
      var dateTime = DateTime.parse(value);
      if (DateTime.now().year - dateTime.year < 18) {
        return "18 Yaşından büyük olmanız gerekmektedir.";
      } else {
        return null;
      }
    }
  }
}

class ColorHelper {
  static Color _backGroundColor;
  static Color _headColor;

  static setBackGroundColor(String hexCode) =>
      _backGroundColor = hexToColor(hexCode);

  static Color get backGroundColor => _backGroundColor;

  static setHeadColor(String hexCode) => _headColor = hexToColor(hexCode);

  static Color get headColor => _headColor;

  static Color hexToColor(String hexColorCode) =>
      Color(_getColorFromHex(hexColorCode));

  static int _getColorFromHex(String hexColor) {
    int result;
    try {
      result = int.parse(_convert(hexColor), radix: 16);
    } catch (e) {
      result = int.parse(_convert("#02375F"), radix: 16);
    }
    return result;
  }

  static String _convert(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll("#", "");
    if (hexColor.length == 6) {
      hexColor = "FF" + hexColor;
    }
    return hexColor;
  }
}
