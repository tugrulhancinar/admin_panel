import 'package:flutter/material.dart';
import 'package:admin_panel/core/extension/context_extension.dart';

class MyButton extends StatelessWidget {
  final String txt;
  final VoidCallback onPressed;
  const MyButton({Key key, @required this.txt, @required this.onPressed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: context.width / 4,
      child: ElevatedButton(
        onPressed: onPressed,
        child: Padding(
          padding: context.paddingLow,
          child: Text(txt),
        ),
      ),
    );
  }
}
