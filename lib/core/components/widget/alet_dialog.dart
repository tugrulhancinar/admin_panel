import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CustomAlertDialog extends StatelessWidget {
  final String baslik;
  final String bodyText;
  final List<Widget> actions;
  final Widget content;

  CustomAlertDialog({
    @required this.baslik,
    this.bodyText,
    this.actions,
    this.content,
  }) /*: assert(
  bodyText == null && content== null,
  'Hem content hem de body text boş olamaz. Body kısmında bir şey göstermek gerekli',
  )*/
  ;

  Future<bool> goster(BuildContext context) async {
    return showDialog<bool>(
        context: context,
        builder: (context) => this,
        barrierDismissible: false);
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
        title: Text(baslik),
        content: content == null ? Text(bodyText) : content,
        actions: actions == null ? action(context) : actions);
  }

  List<Widget> action(BuildContext context) {
    return [
      TextButton(onPressed: () => Navigator.pop(context), child: Text("Tamam"))
    ];
  }
}
