import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class Theme1{
  static final ThemeData themeData = ThemeData(
    brightness: Brightness.light,
    primaryColor: const Color(0xFF004DA6),
    accentColor:  Colors.orange,
    backgroundColor: const Color(0xFFFFFFFF),
    appBarTheme: AppBarTheme(
      backgroundColor:const Color(0xFF004DA6),
      centerTitle: true,
    ),
    textTheme:TextTheme(
      headline1: GoogleFonts.ubuntu(
          fontSize: 98,
          letterSpacing: -1.5,
          fontWeight: FontWeight.w300
      ),
      headline2: GoogleFonts.ubuntu(
          fontSize: 61,
          letterSpacing: -0.5,
          fontWeight: FontWeight.w300
      ),
      headline3: GoogleFonts.ubuntu(
          fontSize: 49,
          letterSpacing: 0,
          fontWeight: FontWeight.normal
      ),
      headline4: GoogleFonts.ubuntu(
          fontSize: 35,
          letterSpacing: 0.25,
          fontWeight: FontWeight.normal
      ),
      headline5: GoogleFonts.ubuntu(
          fontSize: 24,
          letterSpacing: 0,
          fontWeight: FontWeight.normal
      ),
      headline6: GoogleFonts.ubuntu(
          fontSize: 20,
          letterSpacing: 0.15,
          fontWeight: FontWeight.w500
      ),
      subtitle1: GoogleFonts.ubuntu(
          fontSize: 16,
          letterSpacing: 0.15,
          fontWeight: FontWeight.normal
      ),
      subtitle2: GoogleFonts.ubuntu(
          fontSize: 14,
          letterSpacing: 0.1,
          fontWeight: FontWeight.w500
      ),
      bodyText2: GoogleFonts.ubuntu(
          fontSize: 16,
          letterSpacing: 0.5,
          fontWeight: FontWeight.normal
      ),
      bodyText1: GoogleFonts.ubuntu(
          fontSize: 14,
          letterSpacing: 0.25,
          fontWeight: FontWeight.normal
      ),
      button: GoogleFonts.ubuntu(
          fontSize: 14,
          letterSpacing: 1.25,
          fontWeight: FontWeight.w500
      ),
      caption: GoogleFonts.ubuntu(
          fontSize: 12,
          letterSpacing: 0.4,
          fontWeight: FontWeight.normal
      ),
      overline: GoogleFonts.ubuntu(
          fontSize: 10,
          letterSpacing: 1.5,
          fontWeight: FontWeight.normal
      ),
    ),
    buttonTheme: ButtonThemeData(
      textTheme: ButtonTextTheme.primary,
      buttonColor: const Color(0xFF004DA6),
    ),
  );
  /// İOS TEMA
  static final CupertinoThemeData cupertinotheme = CupertinoThemeData(
  );
}