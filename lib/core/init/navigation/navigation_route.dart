import 'package:admin_panel/core/constant/navigation/navigation_constants.dart';
import 'package:admin_panel/product/model/data_model/vefat_model.dart';
import 'package:admin_panel/product/view/pages/edit_item_page.dart';
import 'package:admin_panel/product/view/pages/item_add_page.dart';
import 'package:admin_panel/product/view/pages/home_page.dart';
import 'package:admin_panel/product/view/pages/login_page.dart';
import 'package:admin_panel/product/view/pages/notfound_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class NavigationRoute {
  static NavigationRoute _instance = NavigationRoute._init();

  static NavigationRoute get instance => _instance;

  NavigationRoute._init();

  Route<dynamic> generateRoute(RouteSettings args, {Object data}) {
    switch (args.name) {
      case NavigationConstans.LOGIN_PAGE:
        return normalNavigate(LoginPage());

      case NavigationConstans.ADD_ITEM_PAGE:
        return normalNavigate(ItemAddPage());

      case NavigationConstans.HOME_PAGE:
        return normalNavigate(HomePage());

      case NavigationConstans.EDIT_ITEM_PAGE:
        return normalNavigate(
            EditItemPage(vefatModel: args.arguments as VefatModel));

      default:
        return normalNavigate(NotFoundPage());
    }
  }

  MaterialPageRoute normalNavigate(Widget widget) {
    return MaterialPageRoute(builder: (context) => widget);
  }
}
