class NavigationConstans {
  static const HOME_PAGE = "/home_page";

  static const LOGIN_PAGE = "/login_page";

  static const ADD_ITEM_PAGE = "/add_item_page";

  static const EDIT_ITEM_PAGE = "/edit_item_page";
}
