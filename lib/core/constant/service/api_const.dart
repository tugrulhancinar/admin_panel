class ApiConstants {
  static ApiConstants _instace;

  static ApiConstants get instance => _instace ??= ApiConstants._init();

  ApiConstants._init();

  ///*****************************
  String get _baseUrl => "https://isbucakapi.zeplinx.com/api/";

  String get _loginUrl => _baseUrl + "admin/login";

  String get _vefatCreateUrl => _baseUrl + "vefat/create";

  String get _getAllVefatUrl => _baseUrl + "vefat/get";

  String _updateVefatUrl(String vefatID) => _baseUrl + "vefat/update/$vefatID";

  String _deleteVefatUrl(String vefatID) => _baseUrl + "vefat/delete/$vefatID";

  String get _tumMezaliklarUrl => _baseUrl + "mezar/get";

  ///*****************************

  Uri get login => Uri.parse(_loginUrl);

  Uri get vefatCreate => Uri.parse(_vefatCreateUrl);

  Uri get getAllVefat => Uri.parse(_getAllVefatUrl);

  Uri updateVefat(String vefatID) => Uri.parse(_updateVefatUrl(vefatID));

  Uri deleteVefat(String vefatID) => Uri.parse(_deleteVefatUrl(vefatID));

  Uri get tumMezaliklar => Uri.parse(_tumMezaliklarUrl);
}
