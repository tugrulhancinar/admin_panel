import 'package:admin_panel/helper.dart';
import 'package:flutter/material.dart';

///Tuğrulhan
extension CustomColorScheme on ColorScheme {
  Color get test => const Color.fromARGB(255, 76, 175, 80);

  Color get customGrey => ColorHelper.hexToColor("#FBFBFB");

  Color get eBelediyeBackGroundColor => ColorHelper.hexToColor("#FCD607");
  Color get customBackGroundColor => ColorHelper.backGroundColor;

  //Color get customBackGroundColor => hexToColor("#02375F");

  Color get customBackGroundLightBlue => ColorHelper.hexToColor("#E3E7EB");

  Color get koyuKirmizi => const Color(0xFFD50000);

  Color get lacivert => const Color(0xFF004DA6);

  Color get customGreen => const Color(0xFF4CB050);

  Color get facebookColor => const Color(0xFF438AFE);

  Color get instagramColor => const Color(0xFFDD2A7B);

  Color get twitterColor => const Color(0xFF2196F3);

  Color get youtubeColor => const Color(0xFFC00000);

  Color get headRenk => ColorHelper.backGroundColor;



  List<Color> get customGradientColors =>
      [lacivert.withOpacity(0.9), lacivert.withOpacity(0.01)];

  List<Color> get customGradientColorsGreen =>
      [Colors.green.withOpacity(0.9), Colors.green.withOpacity(0.01)];

}
